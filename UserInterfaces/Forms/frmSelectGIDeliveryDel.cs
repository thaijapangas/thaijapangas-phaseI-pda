﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UserInterfaces.Execute;
using ObjectManager.ConvertObject;
using ServiceFunction.Cursor;
using MsgBox.ClassMsgBox;

namespace UserInterfaces.Forms
{
    public partial class frmSelectGIDeliveryDel : Form
    {
        #region Constructor
        public frmSelectGIDeliveryDel()
        {
            InitializeComponent();
        }
        #endregion

        #region Member
        DataTable dt_output = new DataTable();
        int No_left = 0;
        int No_Right = 0;
        int No = 0;
        int Row = 0;
        #endregion

        #region Event
        private void frmSelectGIDeliveryDel_Load(object sender, EventArgs e)
        {
            try
            {
                UICursor.CursorWait();

                this.lblMessage.Hide();
                this.txtDO.Enabled = false;
                this.txtSerialNumber.Enabled = false;

                dt_output = (DataTable)Executing.Instance.getGIDeliveryForDel();
                if (dt_output.Rows.Count > 0)
                {
                    //Has Data.
                    this.txtDO.Text = dt_output.Rows[0][0].ToString();
                    this.txtSerialNumber.Text = dt_output.Rows[0][1].ToString();

                    this.lblCountData.Text = dt_output.Rows.Count.ToString() + "/" + dt_output.Rows.Count.ToString() + "/" + "0";
                }
                else
                {
                    //Not found data.
                    this.lblMessage.Show();
                    lblMessage.Text = "NOT FOUND DATA";
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                else
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        private void frmSelectGIDeliveryDel_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Left:
                    lnkPre_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Right:
                    lnkNext_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lnkExit_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Down:
                    lnkDel_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Interval = 2000;
            this.lblMessage.Hide();
        }

        private void lnkExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void lnkPre_Click(object sender, EventArgs e)
        {
            #region Comment
            //if (Row == 0)
            //{
            //    Row = 1;
            //}
            //else
            //{
            //    Row += 1;
            //    if (dt_output.Rows.Count == Row)
            //    {
            //        Row -= 1;
            //    }
            //}

            //if (No == 1)
            //{
            //    return;
            //}
            //if (No == 0)
            //{
            //    No = dt_output.Rows.Count - 1;
            //}
            //else
            //{
            //    No -= 1;
            //}

            //if (No_left == 1)
            //{
            //    No_Right = 0;

            //    this.txtDO.Text = dt_output.Rows[dt_output.Rows.Count - No_left]["do_no"].ToString();
            //    this.txtSerialNumber.Text = dt_output.Rows[dt_output.Rows.Count - No_left]["serial_number"].ToString();

            //    this.lblCountData.Text = No_left + "/" + dt_output.Rows.Count.ToString() + "/" + "0";
            //    return;
            //}
            //if (No_left == 0)
            //{
            //    No_left = dt_output.Rows.Count - 1;
            //}
            //else if (No_left != dt_output.Rows.Count)
            //{
            //    No_left -= 1;
            //}
            #endregion

            if (No == 1)
            {
                this.txtDO.Text = dt_output.Rows[dt_output.Rows.Count - No]["do_no"].ToString();
                this.txtSerialNumber.Text = dt_output.Rows[dt_output.Rows.Count - No]["serial_number"].ToString();

                this.lblCountData.Text = No + "/" + dt_output.Rows.Count.ToString() + "/" + "0";
                return;
            }
            if (No == 0)
            {
                No = dt_output.Rows.Count - 1;
            }
            else if (No != dt_output.Rows.Count)
            {
                No -= 1;
            }
            else if (No == dt_output.Rows.Count)
            {
                No -= 1;
            }
           
            //this.txtDO.Text = dt_output.Rows[Row]["do_no"].ToString();
            //this.txtSerialNumber.Text = dt_output.Rows[Row]["serial_number"].ToString();

            this.txtDO.Text = dt_output.Rows[dt_output.Rows.Count - No]["do_no"].ToString();
            this.txtSerialNumber.Text = dt_output.Rows[dt_output.Rows.Count - No]["serial_number"].ToString();

            this.lblCountData.Text = No + "/" + dt_output.Rows.Count.ToString() + "/" + "0";
        }
        private void lnkNext_Click(object sender, EventArgs e)
        {
            #region Comment
            //if (No_Right == 0)
            //{
            //    No_left = 0;
            //    if (No_Right != dt_output.Rows.Count)
            //    {
            //        No_Right += 1;
            //    }
            //}
            //if (No_Right != dt_output.Rows.Count)
            //{
            //    No_Right += 1;
            //}
            #endregion

            if (No == 0)
            {
                return;
            }
            if (No != dt_output.Rows.Count)
            {
                No += 1;
            }

            this.txtDO.Text = dt_output.Rows[dt_output.Rows.Count - No]["do_no"].ToString();
            this.txtSerialNumber.Text = dt_output.Rows[dt_output.Rows.Count - No]["serial_number"].ToString();

            this.lblCountData.Text = No + "/" + dt_output.Rows.Count.ToString() + "/" + "0";
        }
        private void lnkDel_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuDelete fDel = new frmMenuDelete("DEL"))
            {
                fDel.ShowDialog();
            }
        }
        #endregion
    }
}