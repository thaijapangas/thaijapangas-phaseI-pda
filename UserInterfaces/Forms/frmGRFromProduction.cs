﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UserInterfaces.Execute;
using ServiceFunction.Cursor;
using MsgBox.ClassMsgBox;

namespace UserInterfaces.Forms
{
    public partial class frmGRFromProduction : Form
    {
        #region Constructor
        public frmGRFromProduction()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public Action _Action { get; set; }
        #endregion

        #region Enum
        public enum Action
        {
            New,
            Edit
        }
        #endregion

        #region Save
        private bool Save()
        {
            try
            {
                UICursor.CursorWait();

                #region Set Object

                EntitiesTemp.GRProduction.GRProductionEntity ent = new EntitiesTemp.GRProduction.GRProductionEntity();
                ent.pre_order = this.txtPreOrder.Text.Trim();
                ent.batch = this.txtBatch.Text.Trim();
                ent.serial_number = this.txtSerialNumber.Text.Trim();
                ent.create_date = Executing.Instance.GetDateServer();

                #endregion

                bool _output = Executing.Instance.SaveGRProduction(ent);
                if (_output)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        #endregion

        #region Event
        private void frmGRFromProduction_Load(object sender, EventArgs e)
        {
            try
            {
                UICursor.CursorWait();
                this.lblMessage.Hide();

                this._Action = Action.New;
                //Count
                this.lblCountByPreOrder.Text = Executing.Instance.CountGRProductionALL();
                this.txtPreOrder.Focus();
                this.txtPreOrder.SelectAll();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                else
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        private void frmGRFromProduction_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Left:
                    lnkNEW_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lnkExit_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Interval = 2000;
            this.lblMessage.Hide();
        }

        private void lnkExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void lnkNEW_Click(object sender, EventArgs e)
        {
            if (this._Action == Action.New)
            {
                this.txtPreOrder.Text = string.Empty;

                this.txtPreOrder.Focus();
                this.txtPreOrder.SelectAll();
                return;
            }
            else if (this._Action == Action.Edit)
            {
                this.txtPreOrder.Text = string.Empty;
                this.txtPreOrder.Enabled = true;
                this.txtBatch.Text = string.Empty;
                this.txtBatch.Enabled = true;
                this.txtSerialNumber.Text = string.Empty;

                this.txtPreOrder.Focus();
                this.txtPreOrder.SelectAll();
            }
        }

        private void txtPreOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtPreOrder.Text.Trim()))
                {
                    this.lblMessage.Show();
                    this.lblMessage.Text = "Please key pre order.";
                }
                else
                {
                    this._Action = Action.Edit;

                    this.txtPreOrder.Enabled = false;

                    this.txtBatch.Focus();
                    this.txtBatch.SelectAll();
                }
            }
        }
        private void txtBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtBatch.Text.Trim()))
                {
                    this.lblMessage.Show();
                    this.lblMessage.Text = "Please key batch.";
                }
                else
                {
                    this.txtBatch.Enabled = false;

                    this.txtSerialNumber.Focus();
                    this.txtSerialNumber.SelectAll();
                }
            }
        }
        private void txtSerialNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtBatch.Text.Trim()))
                {
                    this.lblMessage.Show();
                    this.lblMessage.Text = "Please key S/N.";
                }
                else
                {
                    try
                    {
                        UICursor.CursorWait();
                        bool _chk_serial = (bool)Executing.Instance.checkGRProductionSerialDuplicate(this.txtSerialNumber.Text.Trim());
                        if (_chk_serial)
                        {
                            //If has in system.
                            this.lblMessage.Show();
                            lblMessage.Text = "Duplicate Data";
                            this.txtSerialNumber.Text = string.Empty;

                            this.txtSerialNumber.Focus();
                            this.txtSerialNumber.SelectAll();
                        }
                        else
                        {
                            //Save
                            if (Save())
                            {
                                this.lblMessage.Show();
                                this.lblMessage.Text = "SAVE DATA";

                                //Count By Pre Order.
                                this.lblCountByPreOrder.Text = Executing.Instance.CountByPreOrderGRProduction(this.txtPreOrder.Text.Trim());

                                this.txtSerialNumber.Text = string.Empty;

                                this.txtSerialNumber.Focus();
                                this.txtSerialNumber.SelectAll();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                        else
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
                    }
                    finally
                    {
                        UICursor.CursorStop();
                    }
                }
            }
        }
        #endregion
    }
}