﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ServiceFunction.Cursor;
using MsgBox.ClassMsgBox;

namespace UserInterfaces.Forms
{
    public partial class frmMenuDistributionSale : Form
    {
        #region Constructor
        public frmMenuDistributionSale()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmMenuDistributionSale_Load(object sender, EventArgs e)
        {
            this.button1.Focus();
        }
        private void frmMenuDistributionSale_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.D1:
                    //llbDistributionSale_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D2:
                    llbGIRefDelivery_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lklLogout_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        private void llbGIRefInvoice_Click(object sender, EventArgs e)
        {

        }
        private void llbGIRefDelivery_Click(object sender, EventArgs e)
        {
            using (Forms.frmGIRefDelivery fGIDelivery = new frmGIRefDelivery())
            {
                fGIDelivery.ShowDialog();
            }
        }
        private void lklLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}