﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ServiceFunction.Cursor;
using UserInterfaces.Execute;
using MsgBox.ClassMsgBox;

namespace UserInterfaces.Forms
{
    public partial class frmGIRefIOEmpty : Form
    {
        #region Constructor
        public frmGIRefIOEmpty()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public Action _Action { get; set; }
        #endregion

        #region Enum
        public enum Action
        {
            New,
            Edit
        }
        #endregion

        #region Save
        private bool Save()
        {
            try
            {
                UICursor.CursorWait();

                #region Set Object
                EntitiesTemp.GIRefIOEmpty.GIRefIOEmptyEntity ent = new EntitiesTemp.GIRefIOEmpty.GIRefIOEmptyEntity();
                ent.do_no = this.txtDO.Text.Trim();
                ent.serial_number = this.txtSerialNumber.Text.Trim();
                ent.create_date = Executing.Instance.GetDateServer();

                #endregion

                bool _output = Executing.Instance.SaveGIRefIOEmpty(ent);
                if (_output)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        #endregion

        #region Event
        private void frmGIRefIOEmpty_Load(object sender, EventArgs e)
        {
            try
            {
                UICursor.CursorWait();
                this.lblMessage.Hide();
                this._Action = Action.New;
                //Count
                this.lblCountByDO.Text = Executing.Instance.CountGIRefIOALL();

                this.txtDO.Focus();
                this.txtDO.SelectAll();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                else
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        private void frmGIRefIOEmpty_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Left:
                    lnkNEW_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lnkExit_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        //TextBox
        private void txtDO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtDO.Text.Trim()))
                {
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key DO.";
                }
                else
                {
                    this._Action = Action.Edit;

                    this.txtDO.Enabled = false;

                    this.txtSerialNumber.Focus();
                    this.txtSerialNumber.SelectAll();
                }
            }
        }
        private void txtSerialNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtSerialNumber.Text.Trim()))
                {
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key S/N.";
                }
                else
                {
                    try
                    {
                        UICursor.CursorWait();
                        //Save Data
                        bool _chk_serial = (bool)Executing.Instance.checkGIRefIOEmptySerialDuplicate(this.txtSerialNumber.Text.Trim());
                        if (_chk_serial)
                        {
                            //If has in system.
                            this.lblMessage.Show();
                            lblMessage.Text = "Duplicate Data";
                            this.txtSerialNumber.Text = string.Empty;

                            this.txtSerialNumber.Focus();
                            this.txtSerialNumber.SelectAll();
                        }
                        else
                        {
                            //Save
                            if (Save())
                            {
                                this.lblMessage.Show();
                                this.lblMessage.Text = "SAVE DATA";

                                //Count By DO.
                                this.lblCountByDO.Text = Executing.Instance.CountGIByDO(this.txtDO.Text.Trim());

                                this.txtSerialNumber.Text = string.Empty;
                                this.txtSerialNumber.Focus();
                                this.txtSerialNumber.SelectAll();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                        else
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
                    }
                    finally
                    {
                        UICursor.CursorStop();
                    }
                }
            }
        }

        private void lnkNEW_Click(object sender, EventArgs e)
        {
            if (this._Action == Action.New)
            {
                this.txtDO.Text = string.Empty;

                this.txtDO.Focus();
                this.txtDO.SelectAll();
                return;
            }
            else if (this._Action == Action.Edit)
            {
                this.txtDO.Text = string.Empty;
                this.txtDO.Enabled = true;
                this.txtSerialNumber.Text = string.Empty;

                this.lblCountByDO.Text = "0";

                this.txtDO.Focus();
                this.txtDO.SelectAll();
            }
        }
        private void lnkExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Interval = 2000;
            this.lblMessage.Hide();
        }
        #endregion
    }
}