﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ServiceFunction.Function
{
    public class EventControls
    {
        #region "Event TextBox"

        public static void txtNumber_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '.' || e.KeyChar == ControlKeysChars.Back))
            {
                e.Handled = true;
            }
        }

        public static void txtNumberDat_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '-' || e.KeyChar == ControlKeysChars.Back))
            {
                e.Handled = true;
            }
        }

        public static void txtNumInt_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (!((e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == ControlKeysChars.Back))
            {
                e.Handled = true;
            }
        }

        #endregion
    }
}
