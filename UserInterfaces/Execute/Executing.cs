﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using EntitiesTemp.GIDelivery;
using EntitiesTemp.GRProduction;
using EntitiesTemp.GIRefIOEmpty;

namespace UserInterfaces.Execute
{
    public class Executing : Connections.Connection, IDisposable
    {
        #region I. Instance
        public static Executing Instance
        {
            get
            {
                Executing instance = new Executing();
                return instance;
            }
        }
        #endregion

        #region 0. getDateTime
        //Get DateTime
        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;

                string sql = "select GETDATE() AS getDateTime";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();

                DataTable dtDateTime = (DataTable)dbManager.ExecuteToDataTable(command);

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 1. GI Ref. Delivery
        //Check Serial Number
        public bool checkGIDeliverySerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GI_Delivery " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SaveGIDelivery(GIDeliveryEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GI_Delivery (serial_number, do_no, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}') ", _ent.serial_number, _ent.do_no, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public DataTable getGIDeliveryForDel()
        {
            try
            {
                DataTable dt_get = new DataTable();

                string sql = "SELECT * " +
                             "FROM GI_Delivery " +
                             "ORDER BY create_date DESC";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                return dt_get;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_row " +
                             "FROM GI_Delivery ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_row"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIByDO(string _do_no)
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_by_do_no " +
                             "FROM GI_Delivery " +
                             "WHERE (do_no='" + _do_no + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_by_do_no"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 2. GR From Production
        public bool checkGRProductionSerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GR_Production " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SaveGRProduction(GRProductionEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GR_Production (serial_number, pre_order, batch, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}','{3}') ", _ent.serial_number, _ent.pre_order, _ent.batch, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        public string CountGRProductionALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gr_row " +
                             "FROM GR_Production ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gr_row"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountByPreOrderGRProduction(string _pre_order)
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_by_pre_order " +
                             "FROM GR_Production " +
                             "WHERE (pre_order = '" + _pre_order + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_by_pre_order"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 3. Distribution Ret
        public bool checkCustCode(string _cust_code)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();

                string sql = "SELECT * " +
                             "FROM customer " +
                             "WHERE (SUBSTRING(cust_id, 1, 3)  = '" + _cust_code + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region 4. GI Ref. IO Empty
        public bool checkGIRefIOEmptySerialDuplicate(string _serial_number)
        {
            try
            {
                DataTable dt_chk_serial = new DataTable();
                string sql = "SELECT * " +
                             "FROM GI_Ref_IO_Empty " +
                             "WHERE (serial_number='" + _serial_number + "') ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_chk_serial = dbManager.ExecuteToDataTable(command);

                if (dt_chk_serial.Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string CountGIRefIOALL()
        {
            string value = string.Empty;
            try
            {
                DataTable dt_get = new DataTable();
                string _count = string.Empty;

                string sql = "SELECT COUNT(*) AS count_gi_io_empty " +
                             "FROM GI_Ref_IO_Empty ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_get = dbManager.ExecuteToDataTable(command);

                if (dt_get.Rows.Count > 0)
                    value = Convert.ToString(dt_get.Rows[0]["count_gi_io_empty"]);

                return value;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SaveGIRefIOEmpty(GIRefIOEmptyEntity _ent)
        {
            bool _result = false;
            try
            {

                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO GI_Ref_IO_Empty (serial_number, do_no, create_date) ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}') ", _ent.serial_number, _ent.do_no, _ent.create_date));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
                throw;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }
        #endregion

        #region G. IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}