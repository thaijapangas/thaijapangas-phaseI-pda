﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UserInterfaces.Forms
{
    public partial class frmMenuScanBarcode : Form
    {
        #region Constructor
        public frmMenuScanBarcode()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmMenuScanBarcode_Load(object sender, EventArgs e)
        {

        }
        private void frmMenuScanBarcode_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.D1:
                    llbDistributionSale_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D2:
                    llbDistributionRet_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D3:
                    //llbRecordSerialLot_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D4:
                    llbGRProduction_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D5:
                    llbGItoProduction_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D6:
                    //llbGI_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D7:
                    //llbGI_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lklLogout_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        private void llbDistributionSale_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuDistributionSale fMenuDisSale = new frmMenuDistributionSale())
            {
                fMenuDisSale.ShowDialog();
            }
        }
        private void llbDistributionRet_Click(object sender, EventArgs e)
        {
            using (Forms.frmDistributionRet fDisRet = new frmDistributionRet())
            {
                fDisRet.ShowDialog();
            }
        }
        private void llbGRRefPO_Click(object sender, EventArgs e)
        {

        }
        private void llbGRProduction_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuGRProduction fGRProduction = new frmMenuGRProduction())
            {
                fGRProduction.ShowDialog();
            }
        }
        private void llbGItoProduction_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuGIProduction fGIProduction = new frmMenuGIProduction())
            {
                fGIProduction.ShowDialog();
            }
        }
        private void llbTransfer_Click(object sender, EventArgs e)
        {

        }
        private void llbPhysicalCount_Click(object sender, EventArgs e)
        {

        }
        private void lklLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}