﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using UserInterfaces.Execute;

namespace UserInterfaces.Forms
{
    public partial class frmDistributionRet : Form
    {
        #region Constructor
        public frmDistributionRet()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        private void frmDistributionRet_Load(object sender, EventArgs e)
        {
            this.lblMessage.Hide();

            this.txtCustomer.Focus();
            this.txtCustomer.SelectAll();
        }
        private void frmDistributionRet_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Left:
                    lnkNEW_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Right:
                    lnkDelete_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lnkExit_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Down:
                    lnkPrint_Click(sender, e);
                    break;
                default:
                    break;
            }
        }
        //TextBox
        private void txtCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtCustomer.Text.Trim()))
                {
                    //strinf Empty
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key cust code.";
                }
                else if (this.txtCustomer.Text.Length != 7)
                {
                    //7 Digit
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key cust code 7 digit.";
                }
                else if(this.txtCustomer.Text.Length == 7)
                {
                    //Check Cust Code
                    string _sub_cust = this.txtCustomer.Text.Substring(0, 3);
                    if (Executing.Instance.checkCustCode(this.txtCustomer.Text.Substring(0, 3)))
                    {
                        //Has
                        
                    }
                    else
                    {
                        //Not Has
                        this.txtCustomer.Text = string.Empty;
                        this.txtCustomer.Enabled = true;
                        
                        this.lblMessage.Show();
                        this.lblMessage.Text = "Cust :101XXXX";

                        this.txtCustomer.Focus();
                        this.txtCustomer.SelectAll();
                    }
                }
            }
        }
        //Timer
        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Interval = 2000;
            this.lblMessage.Hide();
        }

        private void lnkExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void lnkNEW_Click(object sender, EventArgs e)
        {
            this.txtCustomer.Text = string.Empty;
            this.txtCustomer.Enabled = true;

            this.txtDoc.Text = string.Empty;
            this.txtDoc.Enabled = true;

            this.txtVehicle.Text = string.Empty;
            this.txtVehicle.Enabled = true;

            this.txtSelect.Text = string.Empty;
            this.txtSelect.Enabled = true;

            this.txtCustomer.Focus();
            this.txtCustomer.SelectAll();
        }
        private void lnkDelete_Click(object sender, EventArgs e)
        {
            //Check : ว่ามีอยู่ใน Table หรือไม่ ?

            using (frmSelectDistributionRetDel fDistributionRetDel = new frmSelectDistributionRetDel())
            {
                fDistributionRetDel.ShowDialog();
            }
        }
        private void lnkPrint_Click(object sender, EventArgs e)
        {

        }
        #endregion
    }
}