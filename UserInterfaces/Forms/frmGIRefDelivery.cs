﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using UserInterfaces.Execute;
using MsgBox.ClassMsgBox;
using ServiceFunction.Cursor;

namespace UserInterfaces.Forms
{
    public partial class frmGIRefDelivery : Form
    {
        #region Constructor
        public frmGIRefDelivery()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public Action _Action { get; set; }
        #endregion

        #region Enum
        public enum Action
        {
            New,
            Edit
        }
        #endregion

        #region Save
        private bool Save()
        {
            try
            {
                UICursor.CursorWait();

                #region Set Object
                EntitiesTemp.GIDelivery.GIDeliveryEntity ent = new EntitiesTemp.GIDelivery.GIDeliveryEntity();
                ent.do_no = this.txtDO.Text.Trim();
                ent.serial_number = this.txtSerialNumber.Text.Trim();
                ent.create_date = Executing.Instance.GetDateServer();

                #endregion

                bool _output = Executing.Instance.SaveGIDelivery(ent);
                if (_output)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        #endregion

        #region Event
        //Form
        private void frmGIRefDelivery_Load(object sender, EventArgs e)
        {
            try
            {
                UICursor.CursorWait();
                this.lblMessage.Hide();
                this._Action = Action.New;
                //Count
                this.lblCountByDO.Text = Executing.Instance.CountGIALL();

                this.txtDO.Focus();
                this.txtDO.SelectAll();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                else
                    ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
            }
            finally
            {
                UICursor.CursorStop();
            }
        }
        private void frmGIRefDelivery_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Left:
                    lnkNEW_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Right:
                    lnkDelete_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Up:
                    lnkExit_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Down:
                    lnkPrint_Click(sender, e);
                    break;
                default:
                    break;
            }
        }
        //TextBox
        private void txtDO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtDO.Text.Trim()))
                {
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key DO.";
                }
                else
                {
                    this._Action = Action.Edit;

                    this.txtDO.Enabled = false;

                    this.txtSerialNumber.Focus();
                    this.txtSerialNumber.SelectAll();
                }
            }
        }
        private void txtSerialNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(this.txtSerialNumber.Text.Trim()))
                {
                    this.lblMessage.Show();
                    lblMessage.Text = "Please key S/N.";
                }
                else
                {
                    try
                    {
                        UICursor.CursorWait();
                        //Save Data
                        bool _chk_serial = (bool)Executing.Instance.checkGIDeliverySerialDuplicate(this.txtSerialNumber.Text.Trim());
                        if (_chk_serial)
                        {
                            //If has in system.
                            this.lblMessage.Show();
                            lblMessage.Text = "Duplicate Data";
                            this.txtSerialNumber.Text = string.Empty;

                            this.txtSerialNumber.Focus();
                            this.txtSerialNumber.SelectAll();
                        }
                        else
                        {
                            //Save
                            if (Save())
                            {
                                this.lblMessage.Show();
                                this.lblMessage.Text = "SAVE DATA";

                                //Count By DO.
                                this.lblCountByDO.Text = Executing.Instance.CountGIByDO(this.txtDO.Text.Trim());

                                this.txtSerialNumber.Text = string.Empty;
                                this.txtSerialNumber.Focus();
                                this.txtSerialNumber.SelectAll();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex.InnerException);
                        else
                            ClassMsg.DialogErrorTryCatch("System Error save : ", ex);
                    }
                    finally
                    {
                        UICursor.CursorStop();
                    }
                }
            }
        }
        //Link Label
        private void lnkExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void lnkNEW_Click(object sender, EventArgs e)
        {
            if (this._Action == Action.New)
            {
                this.txtDO.Text = string.Empty;

                this.txtDO.Focus();
                this.txtDO.SelectAll();
                return;
            }
            else if (this._Action == Action.Edit)
            {
                this.txtDO.Text = string.Empty;
                this.txtDO.Enabled = true;
                this.txtSerialNumber.Text = string.Empty;

                this.lblCountByDO.Text = "0";

                this.txtDO.Focus();
                this.txtDO.SelectAll();
            }
        }
        private void lnkDelete_Click(object sender, EventArgs e)
        {
            //Check : ว่ามีอยู่ใน Table หรือไม่ ?
            if (Executing.Instance.CountGIALL() == "0")
            {
                this.lblMessage.Show();
                this.lblMessage.Text = "NOT FOUND DATA";
            }
            else
            {
                using (Forms.frmSelectGIDeliveryDel fDelGI = new frmSelectGIDeliveryDel())
                {
                    fDelGI.ShowDialog();
                }
            }
        }
        private void lnkPrint_Click(object sender, EventArgs e)
        {

        }
        //Timer
        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Interval = 2000;
            this.lblMessage.Hide();
        }
        #endregion
    }
}