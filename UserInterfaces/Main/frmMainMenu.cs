﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UserInterfaces.Main
{
    public partial class frmMainMenu : Form
    {
        #region Constructor
        public frmMainMenu()
        {
            InitializeComponent();
        }
        #endregion

        #region Event
        //Form
        private void frmMainMenu_Load(object sender, EventArgs e)
        {
            this.button1.Focus();
        }
        private void frmMainMenu_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.D1:
                    llbScanBarcode_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D2:
                    //llbRecFG_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D3:
                    llbSendData_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D4:
                    llbDeleteData_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D5:
                    llbDeleteAllData_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.D6:
                    llbDownloadCustomer_Click(sender, e);
                    break;
                case System.Windows.Forms.Keys.Down:
                    lklLogout_Click(sender, e);
                    break;
                default:
                    break;
            }
        }
        //Timer
        private void timer_Tick(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString("dd/MM/yy").Trim();
            lblTimeRun.Text = DateTime.Now.ToString("HH:mm:ss").Trim();
        }
        //Link Label
        private void llbScanBarcode_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuScanBarcode fMscanBarcode = new UserInterfaces.Forms.frmMenuScanBarcode())
            {
                fMscanBarcode.ShowDialog();
            }
        }
        private void llbViewData_Click(object sender, EventArgs e)
        {

        }
        private void llbSendData_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuSendData fSendData = new UserInterfaces.Forms.frmMenuSendData())
            {
                fSendData.ShowDialog();
            }
        }
        private void llbDeleteData_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuDelete fDelete = new UserInterfaces.Forms.frmMenuDelete("DEL"))
            {
                fDelete.ShowDialog();
            }
        }
        private void llbDeleteAllData_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuDelete fDelete = new UserInterfaces.Forms.frmMenuDelete("DEL_ALL"))
            {
                fDelete.ShowDialog();
            }
        }
        private void llbDownloadCustomer_Click(object sender, EventArgs e)
        {
            using (Forms.frmMenuDownloadCustomer fDownloadCustomer = new UserInterfaces.Forms.frmMenuDownloadCustomer())
            {
                fDownloadCustomer.ShowDialog();
            }
        }
        private void lklLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}