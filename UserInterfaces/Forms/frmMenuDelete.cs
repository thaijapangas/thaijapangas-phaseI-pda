﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace UserInterfaces.Forms
{
    public partial class frmMenuDelete : Form
    {
        #region Constrcutor
        public frmMenuDelete(string _pram_menu)
        {
            InitializeComponent();
            _str_menu = _pram_menu.Trim();
        }
        #endregion

        #region Member
        string _str_menu = string.Empty;
        #endregion

        #region Event
        private void frmMenuDelete_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_str_menu))
            {
                if (_str_menu == "DEL")
                {
                    this.lblMenu.Text = "--- ENTER PASSWORD ---";
                }
                else if (_str_menu == "DEL_ALL")
                {
                    this.lblMenu.Text = "--- DELETE ALL DATA ---";
                }
            }
        }
        private void frmMenuDelete_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case System.Windows.Forms.Keys.Up:
                    lklLogout_Click(sender, e);
                    break;
                default:
                    break;
            }
        }

        private void lklLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}